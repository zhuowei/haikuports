SUMMARY="A JSON implementation in C"
DESCRIPTION="
JSON-C implements a reference counting object model that allows you to easily \
construct JSON objects in C, output them as JSON formatted strings and parse \
JSON formatted strings back into the C representation of JSON objects.
"
SRC_URI="https://s3.amazonaws.com/json-c_releases/releases/json-c-0.12-nodoc.tar.gz"
CHECKSUM_SHA256="6fd6d2311d610b279e1bcdd5c6d4f699700159d3e0786de8306af7b4bc94fb35"
HOMEPAGE="https://github.com/json-c/json-c/wiki"
ARCHITECTURES="x86_gcc2"
REVISION="1"
LICENSE="MIT"
COPYRIGHT="
	2009-2012 Eric Haszlakiewicz
	2004, 2005 Metaparadigm Pte Ltd
"

PROVIDES="
	json_c = $portVersion
	lib:libjson_c = $portVersion
	"

REQUIRES="
	haiku >= $haikuVersion
	"

BUILD_PREREQUIRES="
	haiku_devel >= $haikuVersion
	cmd:aclocal
	cmd:autoreconf
	cmd:gcc
	cmd:libtool
	cmd:make
"

SOURCE_DIR="json-c-$portVersion"
PATCHES="json_c-$portVersion.patchset"
BUILD()
{
	sh autogen.sh
	runConfigure ./configure
	make
}

INSTALL()
{
	make install

	prepareInstalledDevelLib libjson-c
	fixPkgconfig libjson-c
	packageEntries devel $developDir
}

TEST()
{
	make check
}

PROVIDES_devel="
	json_c_devel = $portVersion
	devel:libjson_c = $portVersion
	"

REQUIRES_devel="json_c == $portVersion base"
